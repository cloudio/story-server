class UrlMappings {

    static mappings = {

        "/images/${imageId}-${size}.${type}" {
            controller = 'dbContainerImage'
            action = 'index'
        }

        "/$controller/$action?/$id?"{
            constraints {
                // apply constraints here
            }
        }
       // "/savePage"(controller : 'page',action : 'savePage')


        "/"(view:"/index")
        "/photoForm"(view:"/photoForm")
        "500"(view:'/error')
    }
}
