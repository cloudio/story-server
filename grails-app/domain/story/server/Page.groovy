package story.server

import pl.burningice.plugins.image.ast.DBImageContainer

@DBImageContainer(field = 'photo')
class Page {
    int pageNumber
    String content

    static constraints = {
    }

}
